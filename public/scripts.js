$(function() {
    $("h1, h2, h3").each(function(i) {
        var current = $( this );
        var page = current.find( '.page' ).val();
        current.attr("id", "title" + i);
        $( "#toc" ).append(
            "<span>" + page + "</span><a class='" + current.prop("tagName") + "' href='#title" +
            i + "' title='" + current.attr("tagName") + "'>" +
            current.html() + "</a>");
    });
});
